package KUPizza;

public class Drink {
	private int size; // 1=small, 2 = medium, 3=large
	private String flavor; // "Lime", "Cola", etc.
	private double [] prices = {0, 16, 22, 28}; // price of each size

	public Drink(String flavor) { 
		this.flavor = flavor;
	}

	public double getPrice() {
		return prices[size]; // price depends only on the size
	}

	public void setSize(int size) { 
		this.size = size; 
	}

	public String toString() { 
		/* describes this drink */ 
		return String.format("Drink size %d flavor %s and price is %.2f", size, this.flavor, this.getPrice());
		
	}

}
