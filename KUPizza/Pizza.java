package KUPizza;

import java.util.List;

public class Pizza {

	private int size; // 0=nothing, 1=small, 2 = medium, 3=large
	private double [] prices = {0, 120, 160, 200}; // price of each size
	private List<String> toppings;

	public Pizza(int size) { 
		this.size = size;
	}

	public double getPrice() {
		// price depends on the size and number of toppings (30. each)
		return prices[size] + 30*toppings.size();
	}

	public void setSize(int size) { 
		this.size = size; 
	}

	public void addTopping(String topping) { 
		toppings.add(topping);
	}

	public String toString() { 
		/* describes this pizza */ 
		return String.format("Pizza size %d and price is %.2f", size, this.getPrice());
	}

}
