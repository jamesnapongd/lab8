package KUPizza;

import java.util.ArrayList;
import java.util.List;

public class FoodOrder {
	// pizzas in this order
	private List<Pizza> pizzas;
	// drinks in this order
	private List<Drink> drinks;
	
	public FoodOrder() {
		pizzas = new ArrayList<Pizza>( );
		drinks = new ArrayList<Drink>( );
	}
	
	public void addPizza( Pizza pizza ) { pizzas.add( pizza ); }
	public void addDrink( Drink drink ) { drinks.add( drink ); }
	
	public double getTotal() {
		double total = 0;
		for(Pizza p: pizzas) total += p.getPrice();
		for(Drink d: drinks) total += d.getPrice();
		return total;
	}
	
	public void printOrder() {
		for(Pizza p: pizzas) System.out.println( p.toString() );
		for(Drink d: drinks) System.out.println( d.toString() );
		System.out.println("Total price: " + getTotal() );
	}
}
